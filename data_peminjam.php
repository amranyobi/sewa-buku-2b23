<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Data Peminjam</title>
</head>
<body>
	<?php  
		include "config.php";
		$db = new Database();
	?>

	<table border="1">
		<tr>
			<th>No</th>
			<th>Kode Peminjam</th>
			<th>Nama Peminjam</th>
			<th>Jenis Kelamin</th>
			<th>Tanggal Lahir</th>
			<th>Alamat</th>
			<th>Pekerjaan</th>
			<th>Edit</th>
			<th>Hapus</th>
		</tr>
		<?php  
		$no = 1;
		foreach ($db->tampil_data() as $x){
			?>
			<tr>
				<td><?php echo $no++?></td>
				<td><?php echo $x['kode_peminjam']?></td>
				<td><?php echo $x['nama_peminjam']?></td>
				<td><?php 
				if($x['jenis_kelamin']=='L')
						echo "Laki-Laki";
					else 
						echo "Perempuan";
				?></td>
				<td><?php 
				$tanggal_lahir = $x['tanggal_lahir'];
				$ganti_format = date("d-m-Y", strtotime ($tanggal_lahir));
				echo $ganti_format;
				?></td>
				<td><?php echo $x['alamat']?></td>
				<td><?php echo $x['pekerjaan']?></td>
				<td><a href="edit_data_peminjam.php?id=<?php echo $x['kode_peminjam']; ?>">Edit</a></td>
				<td><a href="hapus_data_peminjam.php?id=<?php echo $x['kode_peminjam']; ?>">Hapus</a></td>
			</tr>
			<?php
		}

		?>
	</table>
</body>
</html>
